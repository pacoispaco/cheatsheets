# Cheatsheet for manipulating graphcs and images

## Formats

GIF is (an originally patented) image format that supports lossless data
compression. Original patents expired in 2004. Useful for:

 * sharp-edged line art (such as logos) with a limited number of colors. This
   takes advantage of the format's lossless compression, which favors flat areas
   of uniform color with well defined edges.
 * storing low-color sprite data for games.
 * small animations and low-resolution video clips. GIF supports morphing and
   fade-over effects.


PNG is a non-patented image format that supports lossless data compression.
Originally designed as a replacement for GIF and has better compression but no
support for animation (multiple images).

JPG is an image format with lossy data compression. Usually used for
photographs.

SVG is an XML-based veckor image format for two-dimensional graphics with
support for interactivity and animation.

## Tools

### Gimp

URL: https://www.gimp.org/

### Inkscape

URL: https://inkscape.org/

[Inkscape Open Symbols](https://github.com/Xaviju/inkscape-open-symbols)
contains a lot of useful symbols.

### ImageMagick

URL: https://imagemagick.org/

Contains a suite of command line programs for manipulating images.

Install with:

```
$ sudo apt install imagemagick
```

Programs:

 * conjure - interprets and executes scripts written in the Magick Scripting
   Language (MSL).
 * composite - overlaps one image over another.
 * convert - convert between image formats as well as resize an image, blur,
   crop, despeckle, dither, draw on, flip, join, re-sample, and much more.
 * stream - a lightweight tool to stream one or more pixel components of the
   image or portion of the image to your choice of storage formats.
 * animate - animates an image or image sequence on any X server.
 * display - displays an image or image sequence on any X server.
 * import- saves any visible window on an X server and outputs it as an image
   file. You can capture a single window, the entire screen, or any rectangular
   portion of the screen.
 * mogrify - resize an image, blur, crop, despeckle, dither, draw on, flip,
   join, re-sample, and much more. Mogrify overwrites the original image file,
   whereas, convert writes to a different image file.
 * identify - describes the format and characteristics of one or more image
   files.
 * montage - create a composite image by combining several separate images. The
   images are tiled on the composite image optionally adorned with a border,
   frame, image name, and more.
 * compare - mathematically and visually annotate the difference between an
   image and its reconstruction.

See [ImageMagick: Features and Capabilities](https://www.imagemagick.org/script/index.php) for more info.

## Use cases

### Delete nodes in Inkscape

You can Ctrl + Alt + Click a node to delete it, or select the node and press Delete or Backspace . With either of these options Inkscape will modify the handles on the remaining nodes to try and preserve the shape of the path. If you don't want this to happen, press Ctrl + Delete or Ctrl + Backspace instead.

### Straighten paths in Inkscape

In Inkscape:

 * Select the Edit Path By Nodes tool (F2).
 * Click on your path to select it
 * Ctrl-A to select all the nodes in that path
 * Click Make Selected Segments Lines (on the toolbar at the top - the icon is a straight diagonal line between two square nodes).

### Use and select color palettes (swatches)

In Inkscape, use the **Swatches** tool. There are a number of predefined palettes you can use.

### Create new color palettes (swatches)

In Inkscape, create a new palette with:

1. Open a new document
2. Create some figures and set the colors of them.
3. Save the document as a **GIMP Palette file** (\*.gpl). All colors used in your figures will be saved to the \*.gpl file.
4. Move the document to palette file directory `~/.config/inkscape/palettes` (Linux), `~/inkscape/palettes` (Mac).

Note: The \*.gpl file is a text file you can inspect and edit in your favorite editor. It looks like this:
```
GIMP Palette
Name: My Supercool Palette
#
  0   0   0 #000000
  0  50 117 #003275
  3  37  82 #032552
 12  72 153 #0C4899
 72 134 217 #4886D9
189 217 255 #BDD9FF
235 244 255 #EBF4FF
255 255 255 #FFFFFF
```

This palette file works for GIMP as well. Put it in `~/.config/GIMP/x.y/palettes/` (Linux) where x.y is the GIMP version number.

You can then use the new palette in Inkscape:

1. Restart Inkscape.
2. Open a new or existing Inkscape document.
3. Open the **Swatches** tool. Your palette should now be selectable among the predefined palettes.

### Resize images (with ImageMagick)

Resize all JPG images in current directory to 800 pixels and convert to PNG
format:

```
$ for file in *.jpg; do convert $file -resize 800x $file.png; done
```

### Create animated GIF (with ImageMagick)

Create an animated GIF from all PNG images in current directory, with a 120
millisecond dealy between images and no repeated loop:

```
$ convert -delay 120 -loop 1 *.png animated.gif
```
