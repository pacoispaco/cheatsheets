# Cheetsheat for networking

## Handling local or special host and domain names

Occasionally you will want to handle your own hostname or domain name to IP-adress mapping. This can be done in at least two ways:

 * Editing the `/etc/hosts` file, or
 * Editing a local `~/.hosts` file and setting the `HOSTSALIASES` environment variable.

The latter is nicer. Also, I believe, some upgrades may overwrite the `/etc/hosts` file. E.g:
```
$ echo "128.0.0.42 www.meaningoftheuniverse.org" >> ~/.hosts
$ export HOSTALIASES=~/.hosts
```

Some limitations:

 * `HOSTALIASES` only works for applications using getaddrinfo(3) or gethostbyname(3).
 * For setuid/setgid/setcap applications, libc sanitizes the environment, which means that the `HOSTALIASES` setting is lost. ping is setuid root or is given the net_raw capability upon execution (because it needs to listen for ICMP packets), so `HOSTALIASES` will not work with ping unless you're already root before you call ping.

See: https://unix.stackexchange.com/questions/10438/can-i-create-a-user-specific-hosts-file-to-complement-etc-hosts

## ifconfig and iwconfig

## netstat

Check active Internet connections (only servers):
```
$ sudo netstat -plnt
```

## References

 [1]
