# Cheatsheet for packaging and distributing software

## Debian packages and APT

### Updating info on, and upgrading installed packages

To update info on all packages in APT sources:

```
$ apt-get update
```

To ugrade all installed packages:

```
$ apt-get upgrade
```

### Allow packages that have had their label changed, be updated

```
apt-get --allow-releaseinfo-change update
```

This happened in oktober 2019 when Docker changed the label of Docker from "Docker CE" to "Docker EE" (by mistake). They changed it back a few days later. When running `apt update` I got:
```
...
E: Repository 'https://download.docker.com/linux/ubuntu bionic InRelease' changed its 'Label' value from 'Docker EE' to 'Docker CE'
N: This must be accepted explicitly before updates for this repository can be applied. See apt-secure(8) manpage for details.
Do you want to accept these changes and continue updating from this repository? [y/N]
```
I aborted and ran the above `apt-get --allow-releaseinfo-change update` command which allows the label-change. I then had to run it again when Docker changed the label back to its correct name.

### Useful nonstandard utilities

The program 'apt-file' is not installed by default but is useful for listing files in uninstalled packages. It is installed with:

```
$ apt-get install apt-file
```

### Show information on a package

```
$ apt-cache show <package name>
```

### List dependencies and reverse dependencies of a package

```
$ apt-cache showpkg <package name>
```

### Listing and searching for files in installed packages

To list files of an installed package:

```
$ dpkg -L <package name>
```

To find out which package an installed file belongs to:

```
$ dpkg --search <full-path-to-file>
```

### Listing and searching for files in any package (in APT sources)

To list files of an uninstalled package:

```
$ apt-file list <package name>
```

To find out which package a file exists in:

```
$ apt-file find <filename>
```

To run apt-file you have to update it's index with:

```
$ apt-file update
```

### Cleaning up stuff

Remove packages that were installed by other packages and are no longer needed:

```
$ apt-get autoremove
```

Remove .deb files for packages that are no longer installed on your system:

```
$ apt-get autoclean
```

### List of all installed packages

To generate a list of all installed packages:

```
$ dpkg --get-selections > pkglist.txt
```

Use **dselect** to install packages based on a 'pkglist.txt' file!

```
$ sudo apt-get install dselect
```

Then run:

```
$ sudo dpkg --set-selections < pkglist.txt
$ sudo apt-get dselect-upgrade
```

### Getting information on configured repositories 

To find out which repository a specific installed package comes from:
```
$ apt policy <package name>
```

To list all repositories that are currently used:
```
$ apt-cache policy
```

To get a more compact listing of the reposiroty sites than the command above prints, do:
```
$ apt-cache policy | awk '/http.*amd64/{print$2}' | sort -u
```

## Appimage for Linux

[AppImage](https://appimage.org/) is a Linux distribution agnostic packaging system for distributing and installing softare on Linux.

## Snappy for Linux

Snappy is a software deployment and package management system originally designed and built by Canonical for the Ubuntu phone operating system. The packages, called snaps and the tool for using them, snapd, work across a range of Linux distributions allowing distro-agnostic upstream software packaging. The system is designed to work for internet of things, cloud and desktop computing.

### References

* [Wikipedia article on Snappy](https://en.wikipedia.org/wiki/Snappy_(package_manager)
* [Snapcraft](https://snapcraft.io/) is the tool to package software in Snap packages.
