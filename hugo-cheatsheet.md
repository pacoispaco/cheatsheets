Cheatsheet for Hugo
===================

General
-------

Hugo is written in Go. To install:
```
$ sudo apt-get install hugo
```

Here are some relevant links and documentation:

 1. [Hugo homepage](https://gohugo.io/)

 2. [Hugo's Directory Structure Explained](https://www.jakewiesler.com/blog/hugo-directory-structure/)

Basic stuff
-----------

To check the version:
```
$ hugo version
```

To create a new website from scratch:
```
$ hugo new site quickstart
```

The above will create a new Hugo site in a folder named quickstart.

See https://themes.gohugo.io for a list of themes to consider.

Serving a Hugo site locally
===========================

Goto root directory of site and run:
```
hugo server -D
```

Pushing a Hugo site to Digital Ocean
====================================


