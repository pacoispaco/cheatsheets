# Cheetsheat for gcc and ld

## Inspecting libraries

To list symbol names in a library archive file:
```
$ nm libeglut.a
```

## Check dependencies

To check dependencies of shared libraries:
```
$ ldd /usr/lib/libfoobar-1.so
```
