# Cheetsheat for writing documents

This cheetsheat contains info on working with different types of text
documents.

## General

Use textbased formats for documents.

Only use "rich text" formats like LibreOffice's odt and Microsoft's docx
formats if it is neccessary for interaction with others. Even then and if
possible, try to generate files with those formats from "source" files that
use textbased formats.

When distributing documents use plain textbased or PDF-format, unless those
recieving the document expect to edit it in LibreOffice or Word.

## Formats

Plain text is ok for shorter documents like quick notes.

Markdown/up formats are better than plain text, but be aware that there are
many variants that differ only in detail.

Tex/Latex is great for creating beautiful typesetting but is impractical in many
project and business settings due to lack of competence.

Asciidoc is neat but the tools are somewhat lacking.

## Editing and build processes

**Documents you own**

Text formats should be edited in your favorite editor.

A build process for documents you own, should look something like this:
```
Source text \\
          [Merge and transformation tool] -> PDF | HTML | ePUB
Template & style /
```

You could add .odt and .docx to the output formats.

**Documents you share ownership with**

If the others only use LibreOffice or Word, you're stuffed.

## Scenarios

### Combine PDF-documents into a single PDF

To combine PDF:s to a single PDF:
```
$ pdfunite doc-1.pdf doc-2.pdf final.pdf
```

### Export animations in LibreOffice Impress to PDF

This does not work out of the box (with LibreOffice 7.0.2). Install this
extension: https://github.com/monperrus/ExpandAnimations

### Split PDF-document into multiple PDF-documents

Use [PDF Arranger](https://github.com/pdfarranger/pdfarranger). It can be
installed with:
```
$ sudo apt install pdfarranger
```

## Tools

### MarkDown

There are many flavors. Pick one and use it.

To generate ToC:s (Table of Contents) use:

  https://github.com/mzlogin/vim-markdown-toc

### Proselint

[proselint](https://github.com/amperser/proselint) is a CLI application for
linting prose!

To install:
```bash
$ pip install proselint
```

### grip

[grip](https://github.com/joeyespo/grip) is a CLI server application for
previewing Github markdown files locally in your browser before pushing to
github.

To install:
```bash
$ pip install grip
```

### markdown-pdf

[markdown-pdf](https://github.com/alanshaw/markdown-pdf) is a tool for
converting markdown files to PDF via HTML & CSS.

To Install:
```bash
$ npm install -g markdown-pdf
```

To use:
```bash
$ markdown-pdf foobar.md -o foobar.pdf
```

###

[landslide](https://github.com/adamzap/landslide) is a tool to generate
slideshows in HTML from Markdown files.

To install:
```
$ pip install landslide
```

### Pandoc

[pandoc](https://pandoc.org) is a swiss army knife for converting document
formats which uses latex.

```bash
$ apt-get install pandoc
```

Generate PDF from md:
```bash
$ pandoc FILENAME.md -o FILENAME.pdf
```

### Texlive

[texlive](https://www.tug.org/texlive/) is a full Tex package.

To install:

```bash
$apt-get install texline
```

### Asciidoctor

asciidoctor is a tool for working with asciidoc files.

To install:
```bash
$ apt-get install asciidoctor
```

### LibreOffice

To control the text encoding (e.g. UTF-8 or iso-8859-1/latin-1) when you save a
file, you need to specify that in the **Export text files** dialog. Go to:
```
[File] - > [Save as...]
```

In the **Save** dialog check:

 - [x] Edit filter settings

And then select [Save]. You will then be presented with the **Export text files**
dialog where you can specify the encoding to use.

 [1] https://superuser.com/questions/1102217/how-to-change-libreoffice-default-text-encoding

## References

 [1] http://www.vogella.com/tutorials/AsciiDoc/article.html

 [2] http://asciidoctor.org/docs/editing-asciidoc-with-live-preview/
