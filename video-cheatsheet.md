# Cheatsheet for video stuff


## Video conferencing

### Zoom

Zoom is not an open-source application, and it is not included in the Ubuntu repositories. We’ll install Zoom from their APT repositories.

Download the latest Zoom deb package:
```
$ wget https://zoom.us/client/latest/zoom_amd64.deb
```

Install with:
```
$ sudo apt install ./zoom_amd64.deb
```

You will be prompted to enter your password.

When a new version is released, to update Zoom, repeat the same steps.

Reference: https://linuxize.com/post/how-to-install-zoom-on-ubuntu-20-04/

## Scenario: Convert Sony RX10 IV video to format with working sound on Instagram

With the movie-file SONRX10.mp4:
```
$ ffmpeg -i SONYRX10.mp4 -loop 1 -c:a aac -b:a 256k -ar 44100 -c:v libx264 -pix_fmt yuv420p -preset faster -tune stillimage -shortest instagram.mp4
```

## Scenario: Reduce size of large mp4 video file

With ffmpeg:
```
$ ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4
```

See: https://unix.stackexchange.com/questions/28803/how-can-i-reduce-a-videos-size-with-ffmpeg

Slack does not handle the codec libx265, so use h264 instead:
```
$ ffmpeg -i C0245.MP4 -vcodec h264 -crf 28 output2.mp4
```

See: https://github.com/AvdLee/RocketSimApp/issues/31

## Scenario: Cut or trim a mp4 video file

With ffmpeg:
```
$ ffmpeg -i input.mp4 -ss 00:05:20 -t 00:10:00 -c:v copy -c:a copy output1.mp4
```

The above command will take the input video input.mp4, and cut out 10 minutes from it starting from 00:05:20 (5 minutes and 20 second mark), i.e. the output video will be from 00:05:20 to 00:15:20.

-ss specifies the starting position and -t specifies the duration from the start position. In the above command, we cut 10 minutes from the 00:05:20 mark.

The -c:v copy -c:a copy commands copy the original audio and video without re-encoding.


See: https://shotstack.io/learn/use-ffmpeg-to-trim-video/
