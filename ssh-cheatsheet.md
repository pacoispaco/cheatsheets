Cheetsheat for ssh
==================

Basic ssh usage
---------------

TBD.

Using a config file for multiple accounts and ssh keys
------------------------------------------------------

Create a user specific ssh config file:

```
vim ~/.ssh/config
```

Add entrys to the config file like this:

```
Host github-project1
    User git
    HostName github.com
    IdentityFile ~/.ssh/github.project1.key
Host github-org
    User git
    HostName github.com
    IdentityFile ~/.ssh/github.org.key
Host github.com
    User git
    IdentityFile ~/.ssh/github.key
```

The identity files are public key files.

References
----------

 [1] "Generating a new SSH key and adding it to the ssh-agent"
     URL: https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/

 [2] "Simplify Your Life With an SSH Config File"
     URL: http://nerderati.com/2011/03/17/simplify-your-life-with-an-ssh-config-file/
