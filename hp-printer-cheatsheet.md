# Cheetsheat for HP-printers

These notes were written for installing a **Color LaserJet Pro MFP M277dw**
printer, so your mileage may vary.

These notes will make your HP printer and scanner work over wireless.

Occasionally the printer and scanner have stopped working when I have done a
kernel upgrade. You then have to reinstall hplip and the plugin.

## Preparations

Preparations

Download the latest install-script, e.g. hplip-3.20.6.run, for hplip at:

* https://developers.hp.com/hp-linux-imaging-and-printing/gethplip

Then chmod it so it's executable. Then download the plugin from:

* https://developers.hp.com/hp-linux-imaging-and-printing/plugins

and download the plugin with the same version number, e.g. hplip-3.20.6-plugin.run and chmod it so it's executable. Also download the associated *.asc file, e.g. hplip-3.20.6-plugin.run.asc.

##  Ubuntu 20.04.1

I intially did not get the scanner to work with hplip-3.20.6 and reported a bug
here: https://bugs.launchpad.net/hplip/+bug/1890143

After the user https://launchpad.net/~valentijn reported it now worked for him, and linked to a [german forum](https://forum.ubuntuusers.de/topic/hp-laserjet-pro-mfp-m277dw-druckt-scannen-geht/), I decided to try installing hplip-3.20.9.

Steps:

 1. Download the hplip-3.20.9.run file and its key file \*.asc. Chmod the run-file
    and then run it.
 2. Download the hplip-3.20.9-plugin.run file aand its key file \*.asc. Chmod
    the run-file and then run it.

Do not install the GUI (which requires the Python 2 lib python-pyqt5) or the Fax functionality (which requires Python 2 lib python-reportlabs)!

```
paco@shannon:~/proj/hp-m277dw/hplip-3.20.9$ ./hplip-3.20.9.run
Creating directory hplip-3.20.9
Verifying archive integrity... All good.
Uncompressing HPLIP 3.20.9 Self Extracting Archive..............................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................

HP Linux Imaging and Printing System (ver. 3.20.9)
HPLIP Installer ver. 5.1

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

Installer log saved in: hplip-install_Fri-13-Nov-2020_12:07:59.log

\
note: Defaults for each question are maked with a '*'. Press <enter> to accept the default.


INSTALLATION MODE
-----------------
Automatic mode will install the full HPLIP solution with the most common options.
Custom mode allows you to choose installation options to fit specific requirements.

Please choose the installation mode (a=automatic*, c=custom, q=quit) : c


INTRODUCTION
------------
This installer will install HPLIP version 3.20.9 on your computer.
Please close any running package management systems now (YaST, Adept, Synaptic, Up2date, etc).


DISTRO/OS CONFIRMATION
----------------------
Distro appears to be Ubuntu 20.04.

Is "Ubuntu 20.04" your correct distro/OS and version (y=yes*, n=no, q=quit) ?


DRIVER OPTIONS
--------------
Would you like to install Custom Discrete Drivers or Class Drivers ( 'd'= Discrete Drivers*,'c'= Class Drivers,'q'= Quit)?   : d

Initializing. Please wait...


SELECT HPLIP OPTIONS
--------------------
You can select which HPLIP options to enable. Some options require extra dependencies.

Do you wish to enable 'Network/JetDirect I/O' (y=yes*, n=no, q=quit) ?
Do you wish to enable 'Graphical User Interfaces (Qt5)' (y=yes*, n=no, q=quit) ? n
Do you wish to enable 'PC Send Fax support' (y=yes*, n=no, q=quit) ? n
Do you wish to enable 'Scanning support' (y=yes*, n=no, q=quit) ?
Do you wish to enable 'HPLIP documentation (HTML)' (y=yes*, n=no, q=quit) ?


ENTER USER PASSWORD
-------------------
Please enter the sudoer (paco)'s password: *********


INSTALLATION NOTES
------------------
Enable the universe/multiverse repositories. Also be sure you are using the Ubuntu "Main" Repositories. See: https://help.ubuntu.com/community/Repositories/Ubuntu for more information.  Disable the CD-ROM/DVD source if you do not have the Ubuntu installation media inserted in the drive.

Please read the installation notes. Press <enter> to continue or 'q' to quit:


SECURITY PACKAGES
-----------------
AppArmor is installed.
AppArmor protects the application from external intrusion attempts making the application secure

Would you like to have this installer install the hplip specific policy/profile (y=yes*, n=no, q=quit) ? y


RUNNING PRE-INSTALL COMMANDS
----------------------------
OK


RUNNING HPLIP LIBS REMOVE COMMANDS
----------------------------------
sudo apt-get remove libhpmud0 libsane-hpaio
sudo apt-get remove libhpmud0 libsane-hpaio ( hp_libs_remove step 1)
OK


MISSING DEPENDENCIES
--------------------
Following dependencies are not installed. HPLIP will not work if all REQUIRED dependencies are not installed and some of the HPLIP features will not work if OPTIONAL dependencies are not installed.
Package-Name         Component            Required/Optional
Do you want to install these missing dependencies (y=yes*, n=no, q=quit) ?


INSTALL MISSING REQUIRED DEPENDENCIES
-------------------------------------
note: Installation of dependencies requires an active internet connection.


CHECKING FOR NETWORK CONNECTION
-------------------------------
Network connection present.


RUNNING PRE-PACKAGE COMMANDS
----------------------------
sudo dpkg --configure -a (Pre-depend step 1)
sudo apt-get install --yes --force-yes -f (Pre-depend step 2)
sudo apt-get update (Pre-depend step 3)
OK


DEPENDENCY AND CONFLICT RESOLUTION
----------------------------------
HPLIP-3.20.3 exists, this may conflict with the new one being installed.
Do you want to ('i'= Remove and Install*, 'q'= Quit)?    :i
Starting uninstallation...
HPLIP uninstallation is completed


RUNNING POST-PACKAGE COMMANDS
-----------------------------
OK


RE-CHECKING DEPENDENCIES
------------------------
OK


RUNNING SCANJET DEPENDENCY COMMANDS
-----------------------------------
sudo apt-get install --assume-yes python-pip (Scanjet-depend step 1)
warning: Failed to install this Scanjet dependency package. Some Scanjet features will not work.
sudo pip2 install --upgrade pip (Scanjet-depend step 2)
sudo apt-get install --assume-yes libleptonica-dev (Scanjet-depend step 3)
sudo apt-get install --assume-yes tesseract-ocr (Scanjet-depend step 4)
sudo apt-get install --assume-yes libtesseract-dev (Scanjet-depend step 5)
sudo -H pip2 install tesserocr (Scanjet-depend step 6)
sudo apt-get install --assume-yes tesseract-ocr-all (Scanjet-depend step 7)
sudo apt-get install --assume-yes libzbar-dev (Scanjet-depend step 8)
sudo apt-get install --assume-yes python-zbar (Scanjet-depend step 9)
warning: Failed to install this Scanjet dependency package. Some Scanjet features will not work.
sudo -H pip2 install opencv-python (Scanjet-depend step 10)
sudo -H pip2 install PyPDF2 (Scanjet-depend step 11)
sudo -H pip2 install imutils (Scanjet-depend step 12)
sudo -H pip2 install pypdfocr (Scanjet-depend step 13)
sudo -H pip2 install scikit-image (Scanjet-depend step 14)
sudo -H pip2 install scipy (Scanjet-depend step 15)
OK


READY TO BUILD AND INSTALL
--------------------------
Ready to perform build and install. Press <enter> to continue or 'q' to quit:


PRE-BUILD COMMANDS
------------------
OK


BUILD AND INSTALL
-----------------
Running './configure --with-hpppddir=/usr/share/ppd/HP --libdir=/usr/lib --prefix=/usr --disable-qt4 --disable-qt5 --enable-doc-build --disable-cups-ppd-install --disable-foomatic-drv-install --disable-libusb01_build --disable-foomatic-ppd-install --disable-hpijs-install --disable-class-driver --disable-udev_sysfs_rules --disable-policykit --enable-cups-drv-install --enable-hpcups-install --enable-network-build --enable-dbus-build --enable-scan-build --disable-fax-build --enable-apparmor_build'
Please wait, this may take several minutes...
Command completed successfully.

Running 'make clean'
Please wait, this may take several minutes...
Command completed successfully.

Running 'make'
Please wait, this may take several minutes...
Command completed successfully.

Running 'sudo make install'
Please wait, this may take several minutes...
Command completed successfully.


Build complete.



POST-BUILD COMMANDS
-------------------


CLOSE HP_SYSTRAY
----------------
Sending close message to hp-systray (if it is currently running)...
OK


HPLIP UPDATE NOTIFICATION
-------------------------
Do you want to check for HPLIP updates?. (y=yes*, n=no) : n


HPLIP PLUGIN UPDATE NOTIFICATION
--------------------------------
HPLIP Plug-in's needs to be installed/updated. Do you want to update plug-in's?. (y=yes*, n=no) : y
Do you want to install plug-in's in GUI mode?. (u=GUI mode*, i=Interactive mode) : i

HP Linux Imaging and Printing System (ver. 3.20.9)
Plugin Download and Install Utility ver. 2.1

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.


HP Linux Imaging and Printing System (ver. 3.20.9)
Plugin Download and Install Utility ver. 2.1

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

(Note: Defaults for each question are maked with a '*'. Press <enter> to accept the default.)


-----------------------------------------
| PLUG-IN INSTALLATION FOR HPLIP 3.20.9 |
-----------------------------------------

  Option      Description
  ----------  --------------------------------------------------
  d           Download plug-in from HP (recommended)
  p           Specify a path to the plug-in (advanced)
  q           Quit hp-plugin (skip installation)

Enter option (d=download*, p=specify path, q=quit) ? q


RESTART OR RE-PLUG IS REQUIRED
------------------------------
If you are installing a USB connected printer, and the printer was plugged in when you started this installer, you will need to either restart your PC or unplug and re-plug in your printer (USB cable only). If you choose
to restart, run this command after restarting: hp-setup (Note: If you are using a parallel connection, you will have to restart your PC. If you are using network/wireless, you can ignore and continue).

Restart or re-plug in your printer (r=restart, p=re-plug in*, i=ignore/continue, q=quit) : i


PRINTER SETUP
-------------
Would you like to setup a printer now (y=yes*, n=no, q=quit) ? y
Please make sure your printer is connected and powered on at this time.
Do you want to setup printer in GUI mode? (u=GUI mode*, i=Interactive mode) : i
Running 'hp-setup  -i' command....

HP Linux Imaging and Printing System (ver. 3.20.9)
Printer/Fax Setup Utility ver. 9.0

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

(Note: Defaults for each question are maked with a '*'. Press <enter> to accept the default.)


--------------------------------
| SELECT CONNECTION (I/O) TYPE |
--------------------------------

  Num       Connection  Description
            Type
  --------  ----------  ----------------------------------------------------------
  0*        usb         Universal Serial Bus (USB)
  1         net         Network/Ethernet/Wireless (direct connection or JetDirect)

Enter number 0...1 for connection type (q=quit, enter=usb*) ? 1

Using connection type: net


Setting up device: hp:/net/HP_Color_LaserJet_MFP_M277dw?ip=192.168.1.17



---------------------
| PRINT QUEUE SETUP |
---------------------


Please enter a name for this print queue (m=use model name:'HP_Color_LaserJet_MFP_M277dw_2'*, q=quit) ?HP_M277dw
Using queue name: HP_M277dw
Locating PPD file... Please wait.

Found PPD file: postscript-hp:10/ppd/hplip/HP/hp-color_laserjet_pro_mfp_m277-ps.ppd
Description:

Note: The model number may vary slightly from the actual model number on the device.

Does this PPD file appear to be the correct one (y=yes*, n=no, q=quit) ?
Enter a location description for this printer (q=quit) ?Arbetsrummet
Enter additonal information or notes for this printer (q=quit) ?

Adding print queue to CUPS:
Device URI: hp:/net/HP_Color_LaserJet_MFP_M277dw?ip=192.168.1.17
Queue name: HP_M277dw
PPD file: postscript-hp:10/ppd/hplip/HP/hp-color_laserjet_pro_mfp_m277-ps.ppd
Location: Arbetsrummet
Information:
error: Cannot setup fax - HPLIP not built with fax enabled.


---------------------
| PRINTER TEST PAGE |
---------------------


Would you like to print a test page (y=yes*, n=no, q=quit) ? y

HP Linux Imaging and Printing System (ver. 3.20.9)
Testpage Print Utility ver. 6.0

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

warning: Unable to connect to dbus. Is hp-systray running?
Printing test page to printer HP_M277dw...
Test page has been sent to printer.

note: If an error occured, or the test page failed to print, refer to the HPLIP website
note: at: http://hplip.sourceforge.net for troubleshooting and support.


Done.

Done.


RE-STARTING HP_SYSTRAY
----------------------

HP Linux Imaging and Printing System (ver. 3.20.9)
System Tray Status Service ver. 2.0

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

warning: GUI Modules PyQt4 and PyQt5 are not installed
error: hp-systray requires Qt4 GUI and DBus support. Exiting.
```

And now for the plugin.

```
paco@shannon:~/proj/hp-m277dw/hplip-3.20.9$ ./hplip-3.20.9-plugin.run
Verifying archive integrity... All good.
Uncompressing HPLIP 3.20.9 Plugin Self Extracting Archive..............................................................

HP Linux Imaging and Printing System (ver. 3.20.9)
Plugin Installer ver. 3.0

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

Plug-in version: 3.20.9
Installed HPLIP version: 3.20.9
Number of files to install: 64

You must agree to the license terms before installing the plug-in:

LICENSE TERMS FOR HP Linux Imaging and Printing (HPLIP) Driver Plug-in

[...]

Do you accept the license terms for the plug-in (y=yes*, n=no, q=quit) ?
Please enter the sudoer (paco)'s password: ********

Done.
```

## Ubuntu 18.04

On my 18.04 Ubuntu machine the latest version of hplip was not available, so I
did not install it with apt-get install. You need to install hplip and a plugin
for the scanner to work.

### Preparations

Download the latest install-script, e.g. **hplip-3.20.6.run**, for hplip at:

 * https://developers.hp.com/hp-linux-imaging-and-printing/gethplip

Then chmod it so it's executable. Then download the plugin from:

 * https://developers.hp.com/hp-linux-imaging-and-printing/plugins

and download the plugin with the same version number, e.g. **hplip-3.20.6-plugin.run**
and chmod it so it's executable. Also download the associated \*.asc file, e.g.
**hplip-3.20.6-plugin.run.asc**.

## Installation of hplip

Download the run file and the key file \*.asc.

Install hplip with (you will be prompted for the sudo password):
```
$ ./hplip-3.20.6.run
(I chose automatic installation mode)
(I selected to install the AppArmour profile for hplip)
(It detected 3.19.11 which I selected to remove and then install 3.20.6)
(When setting up the printer, I selected connection (I/O) type: Network/Ethernet/Wireless network (direvct connection or JetDirect))
```

After this printing should work fine. Now you will need to install the plugin in
order for the scanner to work.

To install the plugin, needed to use the scanner, run:
```
$ sh hplip-3.20.6-plugin.run
```

NOTE: Do not use the `hp-plugin`program! That did not work for me. See below.

If you haven't installed the plugin but start SimpleScan, it will not find the
scanner. When you try to scan, a dialog with the title "Driver Plug-in required"
will appear with the text "HP Device proprietary plug-in is missing. Click 'Next'
to continue plug-in installation. In case of plug-in download failure, run
'hp-plugin' command from command line manually." Clicking on 'Next' failed, so I
ran 'hp-plugin', but that also eventually failed with
```
Plug-in version: 3.20.6
Installed HPLIP version: 3.20.6
Number of files to install: 64

Plugin installation failed
error: Python gobject/dbus may be not installed
error: Plug-in install failed.

Done.
```
