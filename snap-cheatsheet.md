# Cheetsheat for snap

Snap is a distribution independent Linux package manager.

## Installing snap

```
$ sudo apt install snapd
```

## Basic commands

To install a snap:
```
$ sudo snap install <foobar>
```

To uninstall a snap:
```
$ sudo snap remove <foobar>
```

To list which snaps are installed:
```
$ sudo snap list
```

To update all installed snaps:
```
$ sudo snap refresh
```

To search for snaps:
```
$ sudo snap find <foobar>
```

# References

 * [Introductory tutorial on snap](https://tutorials.ubuntu.com/tutorial/basic-snap-usage#0)
