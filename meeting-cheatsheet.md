# Cheetsheat for meetings

## Basics

1. Why & purpose?
2. Expected result?
3. Who?
4. Agenda?
5. Study material?
5. Time & place?
6. Book it!

## Template for invitation

**Purpose / Syfte**:

**Material to read / Material till mötet**:

**Expected result / Resultat från mötet**: This is optional. If possible put the
purpose of the meeting in the invitation subject heading.

Some examples of expected result and purpose:

 * "Decision meeting: ..." / "Beslutsmöte: ..."
 * "Work meeting: ..." / "Arbetsmöte: ..."
 * "Workshop: ..." / "Workshop": ..."
 * "Presentation: ..." / "Presentation ..."
 * "Utvecklingssamtal"

Agenda (English):

1. Introduction & purpose / Inledning & Syfte (Formal notes? Protokoll?) [Name or names]
2. Item A [Everyone]
3. Item B [Everyone]
4. Etc. [Name or names]
3. Conclusion, decisions and actions / Sammanfattning, beslut och åtgärder [Name]

Agenda (Svenska):

1. Inledning och syfte [Namn]
2. Introduktion [Namn]
3. Item A [Alla]
4. Item B [Alla]
5. Sammanfattning, beslut och åtgärder [Namn]

