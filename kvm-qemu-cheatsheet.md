# Readme file for installing KVM/QEMU and Windows 10 on Ubuntu 16.04 and 18.04

My Lenovo Thinkpad T480s came with Windows 10 preinstalled. I originally
installed Ubuntu 16.04.4 over that and have upgraded to 18.04 after that.

## Preparations

### Make sure you've got a license key for Windows 10

Windows license keys used to be printed on a small sticker under the laptop with
Windows preinstalled, but not anymore. Now the license key is embedded in BIOS.
To retrieve the license key you can use one of the programs in the acpica-tools
package. Install it with:

```
$ sudo apt-get install acpica-tools
```

Then run:

```
$ sudo acpidump -n MSDM
```

The last group of five five-letter strings in the output is the key. It should
look something like this:

TK7Y9-1AF49-VY855-LZ7JM-G2Y3Z

Make a note of that!

### Check that your CPU has VT

KVM will only work if the CPU supports hardware virtualization technology (VT),
either Intel VT or AMD-V. To find out whether your CPU supports VT features,
run:

```
$ sudo egrep '(vmx|svm)' /proc/cpuinfo
```

If the output has **vmx** (Intel VT) or **svm** (AMD-V), then your hardware
supports VT, otherwise it does not.

### Get an ISO image of Windows 10

Download an ISO image of Windows 10 for installation from [https://www.microsoft.com/en-in/software-download/windows10ISO](https://www.microsoft.com/en-in/software-download/windows10ISO).

### Decide on the configuration of your VM

You need to decide on how much RAM and disk to give to your VM with Windows 10.
4 GByte RAM and 32 GByte disk space is good. You can always change this later!

Ok, now you're set to go.

## Install KVM/QEMU

To install KVM/QEMU run:

```
$ sudo apt-get install -y qemu-kvm qemu virt-manager virt-viewer libvirt-bin
```

## Install Windows 10

This can be done with the command line program **virt-install** or with the
program **virt-manager** (aka **Virtual machine manager**) which has a GUI.

Run **virt-manager** with (you need to run this as root):

```
$ sudo virt-manager
```

**Note!** In Ubuntu 17.10 this will generate the following message:

```
Invalid MIT-MAGIC-COOKIE-1 keyUnable to init server: Could not connect: Connection refused
Invalid MIT-MAGIC-COOKIE-1 keyUnable to init server: Could not connect: Connection refused
Invalid MIT-MAGIC-COOKIE-1 keyUnable to init server: Could not connect: Connection refused

(virt-manager:3672): Gtk-WARNING **: cannot open display: :0
```

Start virt-manager on Ubuntu 17.10 with:

```
$ virt-manager
```

Select "New Virtual Machine". I chose 4096 GB RAM and 2 CPU:s. My T480s has
8 CPU cores. I chose 32 GB disk and gave my virtual machine the name "windows-10"
and then selected my downloaded ISO image of Windows 10.

Just go through the Windows installer dance and song show!

Once you click inside the QEMU window with the mouse, the focus of the mouse
will stay inside the QEMU window. To release the focus press **Ctrl**+**alt**.

It seems the Windows VM will not automatically detect your graphics card/GPU but
will run with some generic Windows display driver. This is ok for all normal
Windows use, but if you plan to run games or other graphics intensive programs
you should look at configuring your VM to use your graphics card/GPU. I don't
know how to do that yet!

## Adding support for 2560 x 1440 display resolution in the guest OS

KVM/QEMU uses a software BIOS called [seabios](https://github.com/qemu/seabios).

## Installing VirtualBox on the same machine as KVM/QEMU is installed

VirtualBox won't run while the KVM modules are loaded in memory. KVM can work
fine alongside VirtualBox, so it's a one-way problem, but you need to solve it
if you want to be able to run and use both at different times.

This how-to assumes you already hav LVM/QEMU installed and working, and it is
written based on my experiences with Ubuntu 18.04. This how to is very much
based on [4], [5] and [6].


## References

 1. [Tech Republic: How to install Windows 10 in a VM on a Linux machine](https://www.techrepublic.com/article/how-to-install-windows-10-in-a-vm-on-a-linux-machine/).

 2. [Itzgeek: Install KVM (QEMU) on Ubuntu 16.04 / Ubuntu 14.04](https://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/install-kvm-qemu-on-ubuntu-14-10.html).

 3. [Adangel: Custom Resolutions with QEMU and KVM](https://adangel.org/2015/09/11/qemu-kvm-custom-resolutions/).

 4. [Dedoimedo: Using KVM and VirtualBox side by side](https://www.dedoimedo.com/computers/kvm-virtualbox.html).

 5. [Linuxconfig: How to create and manage KVM virtual machines from CLI](https://linuxconfig.org/how-to-create-and-manage-kvm-virtual-machines-from-cli).
