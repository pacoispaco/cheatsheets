Cheetsheat for jekyll
=====================

General
-------

jekyll is written in Ruby. On Ubuntu 16.04 there are the following packages:

 $ sudo apt-get install jekyll

This will install "ruby" but not "ruby-dev", which contains header fields for
Ruby, and not "bundle" which installs Bundler which is a package manager for
Ruby, and handles "gem"-files. To install them do:

 $ sudo apt-get install ruby-bundler
 $ sudo apt-get install ruby-dev

Setting up a development environment for Github pages (jekyll)
==============================================================

Read:

 https://help.github.com/articles/using-jekyll-as-a-static-site-generator-with-github-pages/

Read:

 https://help.github.com/articles/setting-up-your-github-pages-site-locally-with-jekyll/

Create a "Gemfile" in thr root directory for your jekyll project and add these lines:

 source 'https://rubygems.org'
 gem 'github-pages', group: :jekyll_plugins

Then run:

 $ bundle install

Serving the jekyll site locally
===============================

Run:

 bundle exec jekyll serve
