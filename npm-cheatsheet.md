# Cheatsheet for npm

## Initializing local npm environment for developing a npm package

Initialize local npm environment with:
```
$ npm init
```

This will create the 'package.json' file in the current directory, which is
needed to create a npm package.

## Installing packages

Install package foo, in local node\_modules directory:
```
$ npm install foo
```

Install package foo, globally:
```
$ npm install foo -g
```

Install package foo and make it an explicit dependency to your package, and
which also will be distributed together with your package:
```
$ npm install save
```

Install package foo and make it an explicit dependency for developing your
package, but will not be distributed together wth your package:
```
$ npm install save-dev
```

Install all packages listed in the local file 'packages.json', in local
node\_modules directory:
```
$ npm install
```

References:

 1. [npm-install Install a package](https://docs.npmjs.com/cli/install)

## Checking installed packages

Print list of all installed packages in current directory and globally (-g) but
don't print package dependencies (--depth 0):
```
$ npm list -g --depth 0
```

References:

 1. [npm tricks part 1: Get list of globally installed packages](https://medium.com/@alberto.schiabel/npm-tricks-part-1-get-list-of-globally-installed-packages-39a240347ef0)

