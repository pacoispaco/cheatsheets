# Cheatsheet for Python

## Local environment & pip

For each Python project, create a virtualenv in the root directory:
```
$ virtualenv -p [PYTHON_VERSION] env
```

Eg. for Python 3:
```
$ virtualenv -p python3 env
```

where PYTHON\_VERSION is the Python environment you want to create. If you have multiple Python versions installed write "python" followed by TAB + TAB to see what versions you have. If you omit the -p flag the default Python version will be used. Type "python --version" to see the default version.

To jump into the environment:
```
$ source env/bin/activate
```

To leave the environment:
```
(env) $ deactivate
```

Install Python modules as needed in your project with **pip**, while you are in the local enviroment, of course.

As you add Python modules to your code, add them also to a requirements.txt file. This is a simple text file with the name of each Python module on a single line. This will make it easy for other developers to install the required modules with:
```
(env) $ pip install -r requirements.txt
```

To find out the version of a package installed with pip:
```
(env) $ pip show <packagename>
```

To find out which packages have been installed with pip:
```
(env) $ pip list
```

## Shebang line for scripts

Use this shebang line for Python3:
```
#!/usr/bin/env python3
```

## Idoms and patterns

### Merge two dictionaries

To merge two dictionaries **x** and **y**:
```
z = {**x, **y}
```

This was introduced in Python 3.5 and is described in [PEP 448](https://www.python.org/dev/peps/pep-0448/).

### To check if all values in dictionary equal a give value

To check if all values are None in :
```
all(value == None for value in a_dict.values())
```

## Debugging

Insert the following at the point in code you want to start debug tracing:
```
import pdb
pdb.set_trace ()
```
References:

 1. [*Debugging in Python*](https://pythonconquerstheuniverse.wordpress.com/2009/09/10/debugging-in-python/)

## Using the logging module

If a module makes use of the logging module, you can set the logging level with:
```
import sys
import logging
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
```

## Linting

The classic Python lint tool is **pylint**. It has its limitations. Another lint tool that has been around some time is **pyflakes** which is better. The latest lint tool is **flake8**.

I want whitespace before '(' so i run:
```
(env) $ flake8 --ignore E211
```
References:

 1. [Bye-bye pylint](https://dmerej.info/blog/post/bye-bye-pylint/)
 2. [How I Lint My Python](https://dmerej.info/blog/post/how-i-lint/)
 3. [Flake8: Your Tool For Style Guide Enforcement](http://flake8.pycqa.org/en/latest/)

## iPython

### Installing and virtualenv

To use iPython in a virtualenv you should install it in that virtualenv with pip. This will ensure that the right version of iPython is installed in your virtualenv. Install with:
```
(env) $ pip install ipython
```

### Autoreload of modules

To enable autoreload of modules, enter the following in ipython:
```
In [1]: %load_ext autoreload

In [2]: %autoreload 2
```

In order to set up iPython to have autoreload enabled by default, first create an iPython profile (config file):
```
$ ipython profile create
```

This will typically create a config file here:
```
/home/<username>/.ipython/profile_default/ipython_config.py
```

Note that this config file is outside your virtualenv but will work for all iPython sessions inside your virtualenvs.

Open the above file for editing and make sure the following two settings exist:
```
c.InteractiveShellApp.extensions = ['autoreload']
c.InteractiveShellApp.exec_lines = ['%autoreload 2']
```

References:

 1. [IPython extensions: autoreload](http://ipython.readthedocs.io/en/stable/config/extensions/autoreload.html)

### History and finding previous session input

To view the history of the current session:
```
In [n]: %history
```

To view the history of the current and all previous sessions:
```
In [n]: %history -g
```

To glob (simple grep) history input and see all input lines with "foo":
```
In [n]: %history -g foo
```

References:

 1. [IPython: Built-in magic commands: magic-history](https://ipython.org/ipython-doc/3/interactive/magics.html#magic-history)
