# Cheatsheet for Bash

## Tools: ShellCheck

[ShellCheck](https://github.com/koalaman/shellcheck) is a linter for Bash/sh
scripts.

Install with:
```
$ sudo apt install shellscript
```
