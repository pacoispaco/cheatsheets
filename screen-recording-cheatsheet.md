Cheetsheat for screen recording on Linux
========================================

Purpose and needs:

 a) Capture browser window action.
 b) Capture application interface or full screen action.
 c) Edit video capture by inserting:
    * Text or images.
    * Spoken word.
    * Other video clips.

Short run down of alternatives
------------------------------

**Kazaam**

References
----------

 1. [Best Screen Recorders for Linux](https://itsfoss.com/best-linux-screen-recorders/)
 1. [Record Screen in Ubuntu Linux With Kazam - Beginner’s Guide](https://itsfoss.com/kazam-screen-recorder/)
