Cheetsheat for Eiffel
=====================

Unless otherwise noted, all comments are for Eiffel Software's implementation of Eiffel.

ecf files
---------

XML sucks. And the ecf files are meant to be handled by EiffelStudio, and not by humans with text editor. Also, there is only a specification of the ecf syntax, but not of the semantics.


Principles for working with ecf files
-------------------------------------

When specifying Eiffel libraries with ecf files make sure each library is compilable before including it in other ecf files.
