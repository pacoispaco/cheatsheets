# Howto setup static website with a github repository

This is a setup which is nice if you:

* want to use a static website generator like Hugo or Jekyll,
* want to have the website in a git repository on github,
* want the website to automatically be updated when every you do a push to the master branch in the github repository and
* want to other people, with access to the github repository, be able to edit and update the website.

By using a github repository, you can allow users to edit and commit website files directly in the github web interface and have the website automatically be updated, without having to install git and edit files locally on their machines.

This howto assumes you:

* have a github account,
* have a server with ssh access to, where you will publish your website,
* have a DNS administration account for the domain that will be used for the website and
* have chosen a static website generator to use!

The rest of this howto assumes you have working knowledge of **git**, **github**, **ssh** and generating ssh keys, **nginx** and using nginx with **CGI-scripts** as well as adding **github deploy keys** and **github webhooks**. The server hosting the website is assumed to be a Ubuntu server. It should be easy to use this howto even if you use a different Linux server or other webserver.

## Initial setup of github repository for the website

Start by creating the website locally with the static website generator you have chosen, and edit it until it looks decent enough for you! Let's call the website directory 'www.example.com'.

Create the local git repo for your website and push it to a new github repository with the same name as the website directory.

## Set up the web server

Use ssh to log into your server and install **nginx** and **fcgiwrap**:
```
$ sudo apt install nginx fcgiwrap
```

Install **git** and the static website generator, for example **hugo**:
```
$ sudo apt install git hugo
```

Create the root directory where your website will live:
```
$ sudo mkdir /var/www/website
$ sudo chmod www-data:www-data /var/www/website
```

## Set up the www-data user so it can use github

We will be running a CGI script to clone and pull the github repository. That script will be run by the 'www-data' user so we need to generate a ssh key pair and configure ssh for the 'www-data' user. Finally we will add the public ssh key for the 'www-data' user as a deploy key in the github repository.

Create a .ssh directory and config file for the user 'www-data':
```
$ sudo mkdir /var/www/.ssh
$ sudo chmod www-data:www-data /var/www/.ssh
```

Jump into /var/www/.ssh and create a ssh keypair:
```
$ sudo -uwww-data ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/var/www/.ssh/id_rsa): github_id_rsa
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in github_id_rsa.
Your public key has been saved in github_id_rsa.pub.
The key fingerprint is:
...
```

Now go to the github repository (with your browser) and select **Settings** and then **Deploy keys**. There you select the **Add deploy key** button and enter the public key of the 'www-data' user. You can get the key with:
```
$ sudo cat github_id_rsa.pub
```

You now need to configure ssh for the 'www-data' user so it uses the private ssh key when it runs git commands and communicates with github. Create the file '/var/www/.ssh/config' like this:
```
Host github.com
     User git
     HostName github.com
     IdentityFile /var/www/.ssh/github_id_rsa
```

Now you can test that the 'www-data' user can communicate with github:
```
$ sudo cd /var/www/website
sudo -uwww-data git clone git@github.com:example/www.example.com.git
```

## A CGI bash script for updating the website on the server

This bash script will be invoked as a CGI script and will do a github pull from the github repository 'www.example.com' and the generate the website using the static website generator. Here is a template bash script you can use:
```
#!/bin/bash
# Script that does a git pull on the website github repo and then runs hugo to
# rebuild the website. This script is meant to be run as a CGI script so it
# can be triggered by accessing the URL www.example.com/update/

GITACCOUNT=foobar
GITREPO=www.example.com

# If the repo does not exist locally do a git clone,
# otherwise just do a git pull
if [ -d "$GITREPO" ]; then
  echo "Doing a git pull of $GITREPO"
  cd $GITREPO
  git pull
else
  echo "Doing a git clone of $GITREPO"
  git clone git@github.com:$GITACCOUT/$GITREPO.git
  cd $GITREPO
fi

# Run the hugo rebuild website command
echo "Running hugo rebuild website"
hugo

EOF
```

Copy and save that file as '/var/www/website/update.cgi'. Make sure it is owned by www-data and is executable.

## Set up the website in nginx

Create a niginx configuration file for your website:
```
$ sudo vim /etc/nginx/sites-available/www.example.com
```

and then enable it. It is probably good to redirect users that go to example.com to www.example.com.

It should look like this:
```
server {
    listen 80;
    server_name www.example.com;

    location / {
        alias /var/www/website/www.example.com/public/;
        autoindex on;
    }

    location = /update/ {
        fastcgi_intercept_errors on;
        fastcgi_param DOCUMENT_ROOT     /var/www/website;
        fastcgi_param SCRIPT_NAME       /update.cgi;
        include fastcgi_params;
        fastcgi_pass unix:/var/run/fcgiwrap.socket;
  }
}

server {
    listen       80;
    server_name  example.com;
    return       301 http://www.example.com$request_uri;
}
```

Make sure to restart nginx after you've changed the configuration:
```
$ sudo systemctl reload nginx
```
