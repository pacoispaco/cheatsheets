# Cheatsheet for various fun tools

## Visualization of source code

### Tool: Gource

[Gource](https://gource.io/) is a tool for visualizing git repository history
as an ananimated tree.

Install with:
```
$ sudo apt install gource
```

Generate a mpeg-file with:
```$ gource -s 0.1 -1280x720 --auto-skip-seconds .1 --multi-sampling --stop-at-end --key --highlight-users --hide mouse,progress --file-idle-time 0 --max-files 0 --background-colour 000000 --font-size 22 --title "Paul Cohens development of RARBAC" --output-ppm-stream - --output-framerate 30 | ffmpeg -y -r 30 -f image2pipe -vcodec ppm -i - -b 65536K movie.mp4
```

## Metrics and SLOC

### Tool: sloccount

[sloccount](https://dwheeler.com/sloccount/) is David Wheeler's tool for counting SLOC in various languages and estimating effort and cost using the COCOMO-method.

Install with:
```
$ sudo apt install sloccount
```

### Tool: pygount

[pygount](https://pypi.org/project/pygount/) is similar to sloccount, but uses the pygments package to analyze the source code and consequently can analyze any programming language supported by pygments.

Install with:
```
$ pip install pygount
```

To get the summary SLOC for all code in directory ```foobar``` do:
```
$ pygount foobar | awk '{s+=$1} END {print s}'
```

## File transfer tools

### Tool: transfer.sh

Simple and free file transfer SaaS:

  https://transfer.sh
