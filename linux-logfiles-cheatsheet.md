# Cheatsheet for logfiles

On Ubuntu 20.4 the default configuration of logrotate has no limit on the size
of log files. This can cause the disk to be filled by the system or kernel
suddenly issuing lots of log messages duu to some problem.

To force the logs to rotate and delete automatically if the reach a certain
size, you need to edit the file `/etc/logrotate.d/rsyslog`. Edit the file and
add the line `maxsize NG` where `N` is replace by som enumber. Eg:

```
/var/log/syslog
{
    rotate 7
    daily
    maxsize 1G # add this line
    missingok
    notifempty
    delaycompress
    compress
    postrotate
        /usr/lib/rsyslog/rsyslog-rotate
    endscript
}
```

Reference: https://stackoverflow.com/questions/35638219/ubuntu-large-syslog-and-kern-log-files/35658810
