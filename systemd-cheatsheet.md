Cheetsheat for systemd
======================

Unless otherwise specified, these comments are for systemd on Ubuntu 16.04 or
later.

Basic usage
-----------

Check if systemd is installed and which version it has:
```
$ systemd –-version
```

Inspect the boot process and what took time:
```
$ systemd-analyze
```

Show all services:
```
$ systemd-analyze blame
```

Working with services
---------------------

Systemd uses “units,” which can be services (.service), mount points (.mount),
devices (.device), or sockets (.socket). The same systemctl command manages all
these types of units.

Check status of a specific service:
```
$ service <system-name> status
```

View all available unit files on your system:
```
$ systemctl list-unit-files
```

List all running units:
```
$ systemctl list-units
```

List all failed units:
```
$ systemctl --failed
```

Start a service:
```
$ service [name] start
```

Stop a service:
```
$ service [name] stop
```

References
----------

 [1] https://www.howtogeek.com/216454/how-to-manage-systemd-services-on-a-linux-system/

 [2] https://www.linux.com/learn/understanding-and-using-systemd

 [3] https://ma.ttias.be/learning-systemd/

 [4] https://www.dynacont.net/documentation/linux/Useful_SystemD_commands/
