# Cheetsheat for git

## Reading resources

Pro Git Book online:

  https://git-scm.com/book/en/v2/

Some links on best practices for workflow and commit messages:

  https://wiki.openstack.org/wiki/GitCommitMessages

  https://sethrobertson.github.io/GitBestPractices/

Github stuff:

  https://github.com/tiimgreen/github-cheat-sheet

## Git and github documentation

A clear simple README.md is essential to make it easy to understand what the
repository is about and how to use the code. It should resort in the root
directory. [Here](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2) is a
good README.mdREADME.md  template.

## Basic git usage

Checkout specific branch:
```
$ git checkout <branchname>
```

Create new branch and switch to it (checkout):
```
$ git checkout -b <branchname>
```

List branches:
```
$ git branch
```

Merge branch. First make sure you have switched to the branch you want to
merge *to*. Then tell git which branch you want to merge:
```
$ git checkout <branch-to-merge-to>
$ git merge <branchname>
```

Delete branch:
```
$ git branch -d <branchname>
```

Checkout specific git commit:
```
$ git checkout <SHA-1 hash>
```

Jump back from specific git commit to head:
```
$ git checkout master
```

## Committing

To add only modified and tracked files (and not add any new files):
```
$ git add -u
```

## Adding dependency to other code as git repo

There are two options; git submodule and git subtree.

Submodules can be said to support a component-based development appoach where
you link to an external repo. This is typically nice when the development
life-cycles of the external repo differs from your development life-cycle.

Subtrees can be said to support a system-based development approach where you
actually create a full copy of the other repos source code tree in your repo.
This can be nice when you want to have the life-cycles in lock-step.

To add another git repo as a submodule do:
```
$ git submodule add https://github.com/foobar/scoobydoo
```

See:

1. https://git-scm.com/book/en/v2/Git-Tools-Submodules
2. http://openmetric.org/til/programming/git-pull-with-submodule/

## Undoing or changing recent changes or commit messages

Undo changes in a specific unstaged file:
```
$ git checkout <filename>
```

Undo changes in all unstaged files:A
```
$ git checkout -- .
```

Unstage files from staging area (added):
```
$ git reset
```

Change most recent commit message:

```
$ git commit --amend -m "New commit message"
```

## Visualize git history and branches in command line

Do:
```
$ git log --all --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```

## Scenario: Do some changes and number of commits, and then squash all the commits into one commit

You are working in `master`, and have done a number of changes and commits, that
you know want to squash into a single commit.

There are two ways of doing this as answered in https://stackoverflow.com/questions/5189560/squash-my-last-x-commits-together-using-git

* With `git rebase -i <after-this-commit>` or
* with `git reset --soft <after-this-commit>`.

where <after-this-commit> can be HEAD~N or the hash id of a commit.

### 1. Choose the starting commit

Irrespective of which way you chose you need to decide which commits to squash.

### 2. Reset the HEAD N commits

If you want to squash the last **N** commits do:
```
$ git reset -soft <after-this-commit>
```

Now all your changes in those N commits are preserved, and are ready to be committed as a new single commit.

### 3. Do a new commit

Add the relevant files that you want to commit to the staging area:
```
$ git add ...
```

Commit:
```
$ git commit -m"A sensible commit message"
```

### 4. Push to remote

If you are a) working alone on a project and b) have already pushed the commits
you want to squash to the remote repository (github), you will not be able to
push. You will get this:
```
$ git push
To github.com:pacoispaco/paulcohen.se.git
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to 'git@github.com:pacoispaco/paulcohen.se.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

In order to push your repo to the remote you will need to force push:
```
$ git push -f
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 843 bytes | 843.00 KiB/s, done.
Total 5 (delta 1), reused 2 (delta 0)
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To github.com:pacoispaco/paulcohen.se.git
 + d3d57a9...b0f9330 master -> master (forced update)
```

If you have other devlopers on the project, you will need to do something else.
Maybe look at: https://medium.com/@patrickllgc/git-squash-commits-locally-and-remotely-ca6669efb88

## Scenario: Create branch, do some development and when you are done, squash merge branch into master

You are working in `master`, but want to do some non-trivial development and do
that in a separate branch. When you are done you want to merge the branch into
master as a single commit, without any of the commit logs from the branch.

### 1. Create branch
```
[In master] $ git checkout -b dev-cool-feature-branch
[In dev-cool-feature-branch] $ ... Hacketihack ...
[In dev-cool-feature-branch] $ git add & commit
[In dev-cool-feature-branch] $ ... Hacketihack ...
[In dev-cool-feature-branch] $ git add & commit
[In dev-cool-feature-branch] $ ... Hacketihack ...
[In dev-cool-feature-branch] $ git add & commit
[In dev-cool-feature-branch] $ git checkout master
[In master] $ git merge --squash dev-cool-feature-branch
[In master] $ git commit
```

This creates a single commit from the merged changes from the
dev-cool-feature-branch. Note that there may be multiple commits in the branch.

Omitting the -m parameter in the last commit, lets you modify a draft commit
message containing every message from your squashed commits before finalizing
your commit. You clould clean up that commit message to only say "Add cool
feature".

## Scenario: Create branch with current changes and switch back to original master to do minor fix, commit, and return to branch

You are working in `master` on new stuff, but need to do a fix that has nothing
to do with your new stuff. You want to do the fix and the resume working on new
stuff.

### 1. Create and checkout new branch:
```
[In master] $ git checkout -b new-branch
```
This will hold all current unstaged changes (added and modified files).

### 2. Go back to master and check it out:
```
[In new-branch] $ git branch master
[In master] $ git checkout master
```
This will be the master branch with no unstaged changes.

You now have two choices; a) do your fix in master and commit, or b) create a
branch for your fix, work in that branch and then when you are done merge that
branch with the fix into master.

### 3a. Do fix in master:
```
[In master] ... Hacketihack ...
[In master] $ git add & commit
```

### 3b. Do fix in separate branch:
```
[In master] $ git checkout -b fix-branch
[In fix-branch] ... Hacketihack ...
[In fix-branch] $ git add & commit
[In fix branch] $ git checkout master
[In master] $ git merge fix-branch
```

### 4. Go back to new-branch and resume work, based on the latest master:
```
[In master] $ git checkout new-branch
[In new-branch] $ git rebase master (which may lead to conflicts)
[In new-branch] $ ... Hacketihack ...
[In new-branch] $ git add & commit
[In new-branch] $ git checkout master
[In master] $ git merge new-branch
```

### References

 * [Git Merge vs. Rebase: What’s the Diff?](https://hackernoon.com/git-merge-vs-rebase-whats-the-diff-76413c117333)
 * [Resistance Against London Tube Map Commit History (a.k.a. Git Merge Hell)](http://www.tugberkugurlu.com/archive/resistance-against-london-tube-map-commit-history-a-k-a--git-merge-hell)

## Create a pull request in a central git repo

This should work basically the same way in GithUb and BitBucket.

### References

 * https://docs.github.com/en/free-pro-team@latest/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request
 * https://opensource.com/article/19/7/create-pull-request-github

## Migrate svn repository to git

Use **git-svn**. When installed you can use the command "git svn".

To migrate a svn repository to centrally managed git repository with git-lab, follow these steps.

 1. Clone the svn repo to a local git repo.
```
$ git svn clone <URL-TO-SVN-REPOSITORY>
```
 2. Create a new git project on the git-lab server.

 3. Set origin and push everything as instructed by the git-lab server.

## Remove a file from git history

See: https://stackoverflow.com/questions/43762338/how-to-remove-file-from-git-history
