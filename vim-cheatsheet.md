Cheetsheat for vim
==================

Movement
--------

To go to line N:

<N>G

To junp to the end of a file:

G

Search and replace
------------------

Use the :s (:substitute) commmand.

:%s/foo/bar/g
    Find each occurrence of 'foo' (in all lines), and replace it with 'bar'.

:s/foo/bar/g
    Find each occurrence of 'foo' (in the current line only), and replace it with 'bar'.

:%s/foo/bar/gc
    Change each 'foo' to 'bar', but ask for confirmation first.

:%s/\<foo\>/bar/gc
    Change only whole words exactly matching 'foo' to 'bar'; ask for confirmation.

:%s/\s\\+$//
    Delete all trailing whitespace (at the end of each line) with: Edit

:%s/\sis$//gc
    Delete all trailing " is" strings (Eiffel language change)

Change tabs to space characters
-------------------------------

If expandtab is set then do:

:retab

It will work on the current buffer.

Block selection stuff
---------------------

To comment a block of text:

 1. First, go to the first line you want to comment, press CtrlV. This will put
    the editor in the VISUAL BLOCK mode.
 1. Then using the arrow key and select until the last line.
 1. Now press ShiftI, which will put the editor in INSERT mode and then press #.
    This will add a hash to the first line.
 1. Then press Esc (give it a second), and it will insert a # character on all
    other selected lines.

To uncomment a block of text:

 1. Put your cursor on the first # character, press CtrlV, and go down until the
    last commented line and press x, that will delete all the # characters
    vertically.

Multiple windows
----------------

To split current window vertically:
```
:vsplit
```

To switch between windows:
```
Ctrl+w
```
