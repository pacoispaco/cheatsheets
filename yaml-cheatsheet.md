# Cheatsheet for YAML

## General info

The official page for YAML is: http://yaml.org

The best Python parsers are **ruamel** and **StrictYAML**. StrictYAML uses
ruamel but adds some stricter constraints on YAML syntax. StrictYAML also
has features for validating YAML against a schema. StrictYAML is good and
cool.

**pyKwalify** is another Python module that can parse and validate YAML.

## Tools: Python YAML parsers

[**PyYaml**](https://pyyaml.org) is the default installed YAML parser in Python
3, but seems like an abandoned project.

[**ruamel**](https://pypi.org/project/ruamel.yaml/) is more updated than PyYaml
and supports the latest YAML specifications (1.2 & 1.3).

[**StrictYAML**](https://github.com/crdoconnor/strictyaml) is based on ruamel
but adds stricter constraints on YAML which prevents a few problems which exist
with the YAML syntax. It also has support for specifying and validating schemas.
Documentation on using StrictYAML and schemas is here: http://hitchdev.com/strictyaml/

## Tools: Python YAML validators

[**PyRx**](https://github.com/rjbs/Rx) is a Python implementation of
[**Rx**](http://rx.codesimply.com/) but seems abandoned and has crappy
(non-existent) documentation.

[**pyKwalify**](https://github.com/grokzen/pykwalify) is a Python clone of
[**Kwalify**](http://www.kuwata-lab.com/kwalify/) which is a YAML and JSON
paresr and validator with a slightly different syntax than Rx.
