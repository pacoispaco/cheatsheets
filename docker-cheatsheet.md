# Cheetsheat for Docker

Unless otherwise specified, these comments are for docker on Ubuntu 16.04 or
later.

## Installation

To install Docker CE (Community Edition) se: https://docs.docker.com/install/linux/docker-ce/ubuntu/

**TLDR**:

Make sure everything is updated and upgraded:
```
$ sudo apt update
$ sudo apt upgrade
```

Add Docker's official GPG key:
```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88, by searching for the last 8 characters of the fingerprint (**check the doc above in case the key has been updated!**):
```
$ sudo apt-key fingerprint 0EBFCD88

pub   rsa4096 2017-02-22 [SCEA]
      9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]
```

Install:
```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Verify the installation:
```
$ sudo docker run hello-world
```

Make sure you belong to the group 'docker' so you don't have to sudo:
```
$ sudo usermod -a -G docker $USER
```
You need to reboot (or terminate your user session) for your permissions to be reread.

## Basic usage

Check if docker is installed and which version it has:
```
$ docker version
```

Note: On Ubuntu you can get the short version information with:
```
$ docker --version
```

get help:
```
$ docker help
```

## Working with images

Docker images can be created by writing a Dockerfile and running:
```
$docker build -t mydockerimage
```

The -t flag sets the tag name of the image, making it easier to refer to the
image.

List all images, including intermediate images (-a):
```
$ docker images -a
```

List all images, except intermidate images, but only their numeric IDs:
```
$ docker images -q
```

Show history (commands and layers) of an image
```
$ docker history <ID>
```

Show low level info on an image:
```
$ docker inspect <ID>
```

Remove an image:
```
$ docker rmi <ID>
```

List volumes and mount points in an image:
```
$ docker inspect -f '{{ .Mounts }}' <ID>
```

## Working with containers

Run a container:
```
$ docker run <ID>
```

Run a container in detached mode:
```
$ docker run -d <ID>
```

List all containers, including non running containers (-a):
```
$ docker ps -a
```

List all containers, except non running containers, but only their numeric IDs:
```
$ docker ps -q
```

Show low level info on a container:
```
$ docker inspect <ID>
```

Remove a container:
```
$ docker rm <ID>
```

Show log output in container
```
$ docker logs <ID>
```

# Interacting with containers

To jump into a bash session in a container:
```
$ docker exec -it <ID> bash

```

To backup a Mongodb database in a docker container and make it available on
Docker host:

 1. Check which volume mounts are avaiable in the container.
 1. Jump into the the Docker container.
 1. Run:
```
# mongodump -db <databasename> --out <path-to-output-dir-in-volume-mount-point>
```
 1. Run:
```
# sudo tar pczvf <name-of-gzipfile-to-create> <path-to-output-dir-outside-volume-mount-point>
```
 1. Now you can copy the gzip:ed Mongodb backup to wherever you want!

## Mounting a Windows share (SMB 3) from within a Docker container

You will need to install the cifs-utils package in the Docker image in order to
mount the windows share and directory as a cifs file system:
```
mount -t cifs -o username=USERNAME,password=PASSWORD "//IP_ADRESS/WINDOWS_SHARE_NAME/DIRECTORY_PATH" MOUNT_POINT
```

You also need to run the container in priviledged mode:
```
$ docker run -d --privileged foo-bar
```

See: https://jasonfavrod.com/mounting-windows-file-share-in-docker/

## Working with multi-container applications

This is handled with the tool docker-compose. This tool reads a configuration
file docker-compose.yml that describes which containers must run for the
application to work. To star the application (and its containers):
```
$ docker-compose up -d
```

The flag -d indicates that the containers should run in detached mode.

Stop all containers:
```
$ docker-compose stop
```

See which containers are running (for this application):
```
$ docker-compose ps
```

## Some issues with apt in when building Docker images

You may get warnings like this when running Docker build:
```
debconf: delaying package configuration, since apt-utils is not installed
```

You can ignore them. See: https://stackoverflow.com/questions/51023312/docker-having-issues-installing-apt-utils

## References
