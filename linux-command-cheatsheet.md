# Linux/Unix command cheatsheet

This cheatsheet contains info on various command line tools.

## User administration

Add user:
```
$ sudo adduser username
```

Make user sudo:er:
```
$ usermod -aG sudo username
```

Reference: https://www.digitalocean.com/community/tutorials/how-to-create-a-new-sudo-enabled-user-on-ubuntu-20-04-quickstart

## System info and health

### glances

```
$ man glances
...
DESCRIPTION
        glances is a cross-platform curses-based monitoring tool which aims to present a maximum of
        information in a minimum of space, ideally to fit in a classical 80x24 terminal or higher to
        have additional information. It can adapt dynamically the displayed information depending on
        the terminal size. It can also work in client/server mode. Remote monitoring could be done via
        terminal or web interface.

        glances is written in Python and uses the psutil library to get information from your system.
...
```

Documentation:
 * Homepage: https://nicolargo.github.io/glances/
 * Source code: https://github.com/nicolargo/glances

### df

Reports disk space usage for an entire divice. You can specify device or mount point (directory).

```
$ man df
...
DESCRIPTION
       This manual page documents the GNU version of df. df displays the amount of disk space available
       on the file system containing each file name argument. If no file name is given, the space
       available on all currently mounted file systems is shown. Disk space is shown in 1K blocks by
       default, unless the environment variable POSIXLY_CORRECT is set, in which case 512-byte blocks
       are used.

       If an argument is the absolute file name of a disk device node containing a mounted file system,
       df shows the space available on that file system rather than on the file system  containing the
       device node. This version of df cannot show the space available on unmounted file systems, because
       on most kinds of systems doing so requires very nonportable intimate knowledge of file system
       structures.
...
```

To list the disk space used by the device mounted at `/media/paco/paco-backups` in human readable format:
```
$ df -h /media/paco/paco-backups/
```

### du

```
$ man du
...
DESCRIPTION
       Summarize disk usage of the set of FILEs, recursively for directories.
...
```

To list the disk space used by a directory `proj` and the directories one level below that directory, in human readable format:
```
$ du -h -d1 /media/paco/paco-backups/proj/
```

Documentation:
* https://www.cyberciti.biz/faq/linux-check-disk-space-command/

## Text and data manipulation tools

### sed

sed - stream editor for filtering and transforming text

```
$ man sed
...
DESCRIPTION
       Sed is a stream editor. A stream editor is used to perform basic text transformations on an
       input stream (a file or input from a pipeline). While in some ways similar to an editor which
       permits scripted edits (such as ed), sed works by making only one pass over the input(s), and
       is consequently more efficient. But it is sed's ability to filter text in a pipeline which
       particularly distinguishes it from other types of editors.
...
```

Documentation:
* [Sed - An introduction and Tutorial by Bruce Barnett](https://www.grymoire.com/Unix/Sed.html)

### awk

gawk (GNU awk) - pattern scanning and processing language.

```
$ man awk
...
DESCRIPTION
       Gawk is the GNU Project's implementation of the AWK programming language. It conforms to the
       definition of the language in the POSIX 1003.1 standard. This version in turn is based on the
       description in The AWK Programming Language, by Aho, Kernighan, and Weinberger. Gawk provides
       the additional features found in the current version of Brian Kernighan's awk and numerous
       GNU-specific extensions.
...
```

Documentation:
* [The GNU awk User's manual](https://www.gnu.org/software/gawk/manual/gawk.html)

### jq

jq - Command-line JSON processor

```
$ man jq
       jq can transform JSON in various ways, by selecting, iterating, reducing and otherwise mangling
       JSON documents. For instance, running the command jq ´map(.price) | add´ will take an array of
       JSON objects as input and return the sum of their "price" fields.

       jq can accept text input as well, but by default, jq reads a stream of JSON entities (including
       numbers and other literals) from stdin. Whitespace is only needed to separate entities such as 1
       and 2, and true and false. One or more files may be specified, in which case jq will read input
       from those instead.

       The options are described in the #INVOKING-JQ section; they mostly concern input and output
       formatting. The filter is written in the jq language and specifies how to transform the input file
       or document.
...
```

Documentation:
* Homepage: https://stedolan.github.io/jq/
* Source code: https://github.com/stedolan/jq

### mlr

miller - like awk, sed, cut, join, and sort for name-indexed data such as CSV and tabular JSON.

```
$ man mlr
...
DESCRIPTION
       Miller operates on key-value-pair data while the familiar Unix tools operate on integer-indexed
       fields: if the natural data structure for the latter is the array, then Miller's natural data
       structure is the insertion-ordered hash map. This encompasses a variety of data formats, including
       but not limited to the familiar CSV, TSV, and JSON. (Miller can handle positionally-indexed data
       as a special case.) This manpage documents Miller v5.6.2.
...
```

Documentation:
* Home page and source code: https://github.com/johnkerl/miller
* [Miller Docs](https://miller.readthedocs.io/en/latest/)
