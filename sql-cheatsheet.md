# Cheatsheet for SQL

I don't like relational database systems or SQL.

## General info

## General purpose tools

[DBeaver](https://dbeaver.jkiss.org) is a decent general purpose multi-platform SQL tool that works on Ubuntu/Linux. It is written in Java so you will need a JRE on your machine.

It is not available via the Ubuntu repositories so you either have to do a manual download and install or add the DBbeaver repository. To add the DBeaver repo and then install do:
```
$ wget -O - https://dbeaver.io/debs/dbeaver.gpg.key | sudo apt-key add -
$ echo "deb https://dbeaver.io/debs/dbeaver-ce /" | sudo tee /etc/apt/sources.list.d/dbeaver.list
$ sudo apt-get update
$ sudo apt-get -y install dbeaver-ce
```

When you start DBeaver you will need to select which typ of database server you want to run against. After that you will need to specify which actual server you want to use. You may need to specify user and password credentials depending on how the server is configured. The first time you do this for a particular database server, you will be prompted to download and install relevant drivers for the selected database server. 

References:

 * [Install and configure DBeaver on Linux (Ubuntu)](https://medium.com/@jonas.elan/install-and-configure-dbeaver-on-linux-ubuntu-81db4e38e1f3)
 * [Install and Configure DBeaver on Ubuntu 18.04 / Ubuntu 16.04 / Debian 9](https://computingforgeeks.com/install-dbeaver-on-ubuntu-18-04-ubuntu-16-04-debian-9/)
 * [How to Install dbeaver in Ubuntu 18.04 LTS?](https://qiita.com/shaching/items/697e9d42584fe06b4579)

## MS SQL server

To just look around in MS SQL server databases you can use Microsoft's command-line programs **sqlcmd** and/or **mssql-cli**. Both work on Ubuntu/Linux.

 * [Here is info on installing **sqlcmd**](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools) and [here is a manual](https://docs.microsoft.com/en-us/sql/tools/sqlcmd-utility). **Note!** This toll does not resolve hostnames so you always need to use IP-adresses when using the tool.
 * [Here is info on installing **mssqlcli** with pip](https://docs.microsoft.com/en-us/sql/tools/mssql-cli?view=sql-server-2017).

## Python and pyodbc with MS SQL Server 2017

You will need to install the Microsoft ODBC driver for SQL Server 2017 which is bundled in the **msodbcsql17** package:
 
 * [Installing the Microsoft ODBC Driver for SQL Server on Linux and macOS](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-2017).

Note that as of februari 25 the repository for Ubuntu 18.10 did not have the **msodbcsql17** package. So if you're on Ubuntu 18.10 you need use the 18.04 repository and install **msodbcsql17** from that.
