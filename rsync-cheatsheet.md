Cheetsheat for rsync
====================

Basic usage
-----------

To backup:
```
$ rsync -aPv frompath topath
```

where:

 * **-a** means archive mode (-rlptgoD).
 * **-P** means --partial and --progress.
 * **-v** means verbose.

To dryrun (without actually copying any files) use **-n**:
```
$ rsync -naPv frompath topath
```

References
----------

