# Paul Cohen's tools and cheatsheets

*The Devil lives in the details*

This is an ecclectic collection of cheatsheets and how-tos for tools that I use, have used or want to use. My brain is small and the tools and options are many! Most are in English, but some may be in Swedish.

In no way are the cheatsheets and how-tos complete or even necessarily correct. Your mileage may vary.

## Documentation & editors

 * [Documentation](document-cheatsheet.md)
 * [Images](images-cheatsheet.md)
 * [vim](vim-cheatsheet.md)

## Processes and routines

 * [Meetings](meeting-cheatsheet.md)
 * [Recruiting](recruiting-cheatsheet.md)
 * [Testing](testing-cheatsheet.md)

## Programming & scripting languages

 * [Programming](programming-cheatsheet.md)
 * [Python](python-cheatsheet.md)
 * [C: gcc and ld](gcc-and-ld-cheatsheet.md)
 * [Eiffel](eiffel-cheatsheet.md)
 * [npm](npm-cheatsheet.md)

## Transfer & data file formats

 * [JSON](json-cheatsheet.md)
 * [YAML](yaml-cheatsheet.md)

## API:s

 * [Tools for HTTP API:s](http-api-cheatsheet.md)

## Development, testing & deployment

 * [git](git-cheatsheet.md)
 * [Docker](docker-cheatsheet.md)

## Shells & command line tools

 * [Dotfiles](dotfiles-cheatsheet.md)
 * [Bash](bash-cheatsheet.md)

## Web servers

 * [Nginx](nginx-cheatsheet.md)

## Databases

 * [SQL](sql-cheatsheet.md)

## Operating systems & networking

 * [Networking](networking-cheatsheet.md)
 * [ssh](ssh-cheatsheet.md)
 * [Rsync](rsync-cheatsheet.md)

### Debian/Ubuntu GNU Linux

 * [Debian packackes and APT](debapt-cheatsheet.md)
 * [Snap](snap-cheatsheet.md)
 * [systemd](systemd-cheatsheet.md)
 * [KVM/QEMU](kvm-qemu-cheatsheet.md)

### Android

### Windows

Only if I have to.

## Static websites

 * [Hugo](hugo-cheatsheet.md)
 * [Jekyll](jekyll-cheatsheet.md)
 * [How to setup a static website with github](howto-setup-static-website-with-github.md)

## Other devices

 * [HP printer](hp-printer-cheatsheet.md)
 * [Mobile devices](mobile-devices-cheatsheet.md)
 * [Thinkpad Thunderbolt3 Dock](thinkpad-thunderbolt3-dock-cheatsheet.md)

## Communication and various web services

 * [Mail](mail-cheatsheet.md)

## Images and video

 * [Screen recording](scripting-recording-cheatsheet.md)

## Maps 

 * [Mapping](mapping-cheatsheet.md)
