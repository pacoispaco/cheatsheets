# Cheatsheet for JSON

## General

To pretty print JSON files at the commandline:
```
$ python -m json.tool data-ioc/Anser_fabalis.json
```

## Tools: underscore-cli

underscore-cli is a Node.js applciation that can be installed with
```
$ sudo npm install -g underscore-cli 
```

Sourcecode: https://github.com/ddopson/underscore-cli

To prettyprint a json-file with colors:
```
$ underscore print -i foobar.json  --color
```
