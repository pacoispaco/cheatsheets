# Cheatsheet for mobile app development

## Introduction

Developing mobile apps for Android and iOS is problematic because you either
have to have to code bases, one in Java for Android and one in Objective C for
iOS, or you will need to write your code in some form of transpiler solution or
layered solution like React Native.

Testing mobile apps is another challenge since you may not want to clutter your
mobile phone with dev or beta versions of an app. You therefore need to have a
dedicated iPhone and Android device, or rely on som sort of emulator.

## Tools: Genymotion

[Genymotion](https://www.genymotion.com/thank-you-trial/?opt=ubu_first_64_download_link)
enables you to run virtual Android devices on your desktop or in the cloud and
is useful for testing and integrating in your CI toolchain.

Register and download the installer for Linux, named something like:
```
genymotion-3.0.1-linux\_x64.bin
```

Make the fiel excutable and run it. If you run it normally it will install for
the current user only. If you want it to be installed for all users, run it as
root/sudo.

The installer will complain if you do not have VirtualBox installed:
```
- Trying to find VirtualBox toolset .................... WARNING (Virtualbox was not found in you PATH. Please install it manually)
```

Install VirtualBox. If you already have KVM/QEMU installed you will need to
handle that since you can not run both VirtualBox and KVM/QEMU at the same time.
See []() for instructions on how to have VirtualBox and KVM/QEMU installed and
working on the same machine.

If you install Genymotion as the current user, the installer will install
genymotion in the subdirectory 'genymotion' of the current directory. There are
three programs you can invoke:

 * genymotion
 * genymotion-shell
 * gmtool

As this is a WARNING, I ignored it for now.

## Tools: Transpilers and other layered solutions

[React Native](https://facebook.github.io/react-native/) is a popular layered
solution where the code is written in JavaScript but makes calls to an
underlying interpreter that communicates with the underlying mobile platform.

## Tools: Anbox for Linux

[Anbox](https://anbox.io/) is a free and open-source compatibility layer that
aims to allow mobile applications and mobile games developed for Android to run
on GNU/Linux distributions.

It executes the Android runtime environment by using LXC (Linux Containers),
recreating the directory structure of Android as a mountable loop image, whilst
using the native Linux kernel to execute applications.

Se [https://ebblr.com/how-to-install-anbox-on-ubuntu-18-04/](https://ebblr.com/how-to-install-anbox-on-ubuntu-18-04/)
for information on how to install Anbox on Ubuntu 18.04.

**NOTE**: You need to have started Anbox in order to install APK files:
```
$ anbox session-manager
```
After that you can install APK files with:
```
$ sudo adb install /the/location/of/file.apk
```

## Tools: Android studio

Install with snap:
```
$ sudo snap install android-studio --classic
```
Note that you have to use the --classic flag.

When you start Android Studio it will install all modules and plugins.

### The Android Virtual Device Manager

NOTE: I have not yet got this to work (4/4 2019).

This tool is not trivial to find as a new user. You can't find it the menu but
is should be in the main tool menu bar. The icon is a small mobile phone with
the little green Android guys head on. It should display **AVD Manager** when
you hoover the mouse over it.

When you start the AVD Manager and try to create your first virtual device you
probably will encounter this problem:
```
/dev/kvm device:permission denied
```

This is because youir user doesn't have access to that directory. Use the
adduser command to add your user to the kvm group:
```
$ sudo adduser <username> kvm
```

### Useful command line tools

**adb** is the Android Debug Bridge tool. To list currently attached devices:
```
$ adb devices
```

### Installing an .apk file in the emulator

Go to the directory containing the .apk file and run:
```
$ adb install foobar.apk
```

To reinstall an already installed app run:
```
$ adb install -r foobar.apk
```

References:

 * [Android Studio 3.3 Released! How to Install it in Ubuntu 18.04](http://tipsonubuntu.com/2019/01/16/android-studio-3-3-released-install-ubuntu-18-04/)
