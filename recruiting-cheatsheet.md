Cheetsheat för att rekrytera utvecklare och IT-personal
=======================================================

I detta uppdrag används termen *uppdrag* som samlingsnamn både för *uppdrag*,
*konsultuppdrag*, *tjänst* och *anställning*.

Tjänst och uppdrag
------------------

Bestäm en tydlig och konkret tjänste- eller uppdragstitel.

Skriv en uppdragsbeskrivning. Den bör innehålla:

 a)

 b)

 c)

Intervju(er)
------------

Referenser
----------

Fråga referenser om (för programmerare):

 a) Har personen verkligen arbetat med det de påstår och är det det de vill
    hålla på med? Exempel 1: söker du efter en programmerare fråga om de
    verkligen programmerar och gillar det!

 b) Fråga om personen varit med i en crunchtime eller deathmarch och hur de
    hanterade det?

 c) Fråga om personen hamnat i en teknisk dispyt och hur den personen bidrog
    till att lösa dispyten?

 d) Fråga om personen är ansvarsfull och självgående.

 d) Skulle de rekommendera personen (utan reservationer) eller skulle de
    återanlita personen om de fick chans?

Utvärdering och beslut
----------------------


