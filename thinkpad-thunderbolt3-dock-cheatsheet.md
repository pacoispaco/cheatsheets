# Cheetsheat for Lenovo Thinkpad Thunderbolt 3 Dock

These are notes for getting the Thinkpad TB3 Dock working with a Lenovo T480s
using Christian Kellners Bolt device manager (https://github.com/gicmo/bolt).

These notes should work for other equivalent hardware/os combinations.

## Linux distributions

In Ubuntu 17.10 there was no official Debian package for bolt. You need to
download the source, build and install it manually. Se the sections below;
"Getting Bolt" and "Configure/make/install".

In Ubuntu 18.04 you can install bolt with:
```
$ sudo get-apt install bolt
```

## Important note about the Lenovo T480s

*Important note!* The Lenovo T480s has two USB type C ports on the left hand
side of the laptop, but only one of them is a actual Thunderbolt port (I am
not sure what they were thinking)! It is the second one that is the TB3 port,
i.e. the port with the flash icon on the left side of it. Make sure you plug in
the Dock to the correct TB3 port!

## Getting Bolt

Get the Bolt source code:
```
$ git clone https://github.com/gicmo/bolt.git
```

## Configure/make/install

Check https://github.com/gicmo/bolt/blob/master/INSTALL.md for the latest
correct installation guide!

If you got the source code do:
```
$ meson build
$ ninja -C build
$ ninja -C build test
```

## References

 1. [http://juho.tykkala.fi/Lenovo-Thunderbolt-3-dock-Linux](Lenovo ThinkPad Thunderbolt 3 dock on Debian Linux).

 2. [https://github.com/gicmo/bolt](Thunderbolt 3 device manager).

